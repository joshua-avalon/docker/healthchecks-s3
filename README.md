# joshuaavalon/healthchecks-s3

This is a Docker image based on [linuxserver/healthchecks](https://hub.docker.com/r/linuxserver/healthchecks) with [s3fs](https://github.com/s3fs-fuse/s3fs-fuse).

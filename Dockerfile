ARG HEALTHCHECKS_VERSION=latest

FROM joshuaavalon/healthchecks:${HEALTHCHECKS_VERSION}

ARG S3FS_VERSION=v1.86

RUN apk add --upgrade \
    build-base \
    alpine-sdk \
    fuse \
    fuse-dev \
    automake \
    autoconf \
    git \
    libxml2-dev \
    curl-dev \
    libressl-dev \
    bash

RUN git clone https://github.com/s3fs-fuse/s3fs-fuse.git /tmp/s3fs-fuse
WORKDIR /tmp/s3fs-fuse
RUN git checkout tags/${S3FS_VERSION}
RUN ./autogen.sh
RUN ./configure --prefix=/usr
RUN make && make install

FROM joshuaavalon/healthchecks:${HEALTHCHECKS_VERSION}

ENV S3FS_BUCKET=
ENV S3FS_ARGS=

RUN apk add --upgrade \
    fuse \
    fuse-dev \
    libxml2-dev \
    curl-dev \
    libressl-dev

COPY --from=0 /usr/bin/s3fs /usr/bin/s3fs
COPY root/ /
